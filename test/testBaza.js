const chai = require('chai');
const chaihttp = require('chai-http');
chai.use(chaihttp);
chai.should();
let assert = chai.assert;

const server = require('../index.js')
const Sequelize = require('sequelize');
const sequelize = require('../db.js');
var Student = require("../models/Student.js")(sequelize);
var Grupa = require("../models/Grupa.js")(sequelize);
var Vjezba = require("../models/Vjezba.js")(sequelize);
var Zadatak = require("../models/Zadatak.js")(sequelize);


Vjezba.hasMany(Zadatak, { as: "zadaciVjezbe" });
Grupa.hasMany(Student, { as: "studentiGrupe" });

var studentiOld = []
var grupeOld = []
var zadaciOld = []
var vjezbeOld = []


describe('Testovi baze', function() {
    this.timeout(90000)
    
    this.beforeAll((done)=> {
        
        sequelize.sync().then(()=> {
            return Student.findAll()
        }).then((rez) => {
            rez.forEach(st => {
                studentiOld.push({id:st.id,ime:st.ime,prezime:st.prezime,index:st.index,GrupaId:st.GrupaId})
            })
            return Grupa.findAll()
        }).then((rez)=> {
            rez.forEach(gr => {
                grupeOld.push({id:gr.id,naziv:gr.naziv})
            })
            return Zadatak.findAll()
        }).then((rez)=> {
            rez.forEach(z => {
                zadaciOld.push({id:z.id,naziv:z.naziv,VjezbaId:z.VjezbaId})
            })
            return Vjezba.findAll()
        }).then((rez)=> {
            rez.forEach(v => {
                vjezbeOld.push({id:v.id,naziv:v.naziv})
            })
            sequelize.sync({force:true}).then(() => {
                done()
            })
        })
    })

    this.afterAll((done)=>{
        
        Vjezba.bulkCreate(vjezbeOld)
        .then(()=> {
            return Zadatak.bulkCreate(zadaciOld)
        }).then(()=> {
            return Grupa.bulkCreate(grupeOld)
        }).then(()=> {
            return Student.bulkCreate(studentiOld)
        }).then(()=> {
            done()
        })
    })

    this.afterEach((done)=> {
        sequelize.sync({force:true}).then(()=> done())
    })


    describe('Testovi za POST /student', function() {
        it('Dodavanje jednog studenta s novom grupom', async function() {
            let student = {ime: 'Test', prezime:'Test',index:'1',grupa:'Grupa 1'}
            let odg = await chai.request(server)
                            .post('/student')
                            .set('Content-Type','application/json')
                            .send(JSON.stringify(student))
                
            assert.equal(JSON.parse(odg.text).status,"Kreiran student!", "Nije ispravan odgovor!")

            let studenti = await Student.findAll()
            assert.equal(studenti.length,1, "Nije ispravan broj studenata u bazi!")
            assert.equal(studenti[0].ime,student.ime, "Neispravno ime studenta")
            assert.equal(studenti[0].prezime,student.prezime, "Neispravno prezime studenta")
            assert.equal(studenti[0].index,student.index, "Neispravan index studenta")

            let grupe = await Grupa.findAll()
            assert.equal(grupe.length,1,"Nije ispravan broj grupa u bazi!")
            assert.equal(grupe[0].naziv,student.grupa,"Nije ispravan naziv grupe")
            assert.equal(studenti[0].GrupaId,grupe[0].id,"Neispravno postavljena grupa studentu")
        });
        it('Dodavanje više studenata s istom grupom', async function() {
            let student1 = {ime: 'Test1', prezime:'Test1',index:'1',grupa:'Grupa 1'}
            let student2 = {ime: 'Test2', prezime:'Test2',index:'2',grupa:'Grupa 1'}

            let odg1 = await chai.request(server)
                            .post('/student')
                            .set('Content-Type','application/json')
                            .send(JSON.stringify(student1))

            let odg2 = await chai.request(server)
                            .post('/student')
                            .set('Content-Type','application/json')
                            .send(JSON.stringify(student2))

            assert.equal(JSON.parse(odg1.text).status,"Kreiran student!", "Nije ispravan odgovor!")
            assert.equal(JSON.parse(odg2.text).status,"Kreiran student!", "Nije ispravan odgovor!")
            
            let studenti = await Student.findAll()
            assert.equal(studenti.length,2, "Nije ispravan broj studenata u bazi!")

            assert.equal(studenti[0].ime,student1.ime, "Neispravno ime prvog studenta")
            assert.equal(studenti[0].prezime,student1.prezime, "Neispravno prezime prvog studenta")
            assert.equal(studenti[0].index,student1.index, "Neispravan index prvog studenta")

            assert.equal(studenti[1].ime,student2.ime, "Neispravno ime drugog studenta")
            assert.equal(studenti[1].prezime,student2.prezime, "Neispravno prezime drugog studenta")
            assert.equal(studenti[1].index,student2.index, "Neispravan index drugog studenta")

            let grupe = await Grupa.findAll()
            assert.equal(grupe.length,1,"Nije ispravan broj grupa u bazi!")
            assert.equal(grupe[0].naziv,student1.grupa,"Nije ispravan naziv grupe")
            assert.equal(studenti[0].GrupaId,grupe[0].id,"Neispravno postavljena grupa prvom studentu")
            assert.equal(studenti[1].GrupaId,grupe[0].id,"Neispravno postavljena grupa drugom studentu")

        });
        it("Dodavanje više studenata s različitim grupama", async function() {
            let student1 = {ime: 'Test1', prezime:'Test1',index:'1',grupa:'Grupa 1'}
            let student2 = {ime: 'Test2', prezime:'Test2',index:'2',grupa:'Grupa 2'}

            let odg1 = await chai.request(server)
                            .post('/student')
                            .set('Content-Type','application/json')
                            .send(JSON.stringify(student1))

            let odg2 = await chai.request(server)
                            .post('/student')
                            .set('Content-Type','application/json')
                            .send(JSON.stringify(student2))
            
            assert.equal(JSON.parse(odg1.text).status,"Kreiran student!", "Nije ispravan odgovor!")
            assert.equal(JSON.parse(odg2.text).status,"Kreiran student!", "Nije ispravan odgovor!")

            let studenti = await Student.findAll()
            assert.equal(studenti.length,2, "Nije ispravan broj studenata u bazi!")

            assert.equal(studenti[0].ime,student1.ime, "Neispravno ime prvog studenta")
            assert.equal(studenti[0].prezime,student1.prezime, "Neispravno prezime prvog studenta")
            assert.equal(studenti[0].index,student1.index, "Neispravan index prvog studenta")

            assert.equal(studenti[1].ime,student2.ime, "Neispravno ime drugog studenta")
            assert.equal(studenti[1].prezime,student2.prezime, "Neispravno prezime drugog studenta")
            assert.equal(studenti[1].index,student2.index, "Neispravan index drugog studenta")

            let grupe = await Grupa.findAll()
            assert.equal(grupe.length,2,"Nije ispravan broj grupa u bazi!")
            assert.equal(grupe[0].naziv,student1.grupa,"Nije ispravan naziv grupe prvog studenta")
            assert.equal(grupe[1].naziv,student2.grupa,"Nije ispravan naziv grupe drugog studenta")
            assert.equal(studenti[0].GrupaId,grupe[0].id,"Neispravno postavljena grupa prvom studentu")
            assert.equal(studenti[1].GrupaId,grupe[1].id,"Neispravno postavljena grupa drugom studentu")
        });

        it("Dodavanje više studenata s istim indexom", async function() {
            let student1 = {ime: 'Test1', prezime:'Test1',index:'1',grupa:'Grupa 1'}
            let student2 = {ime: 'Test2', prezime:'Test2',index:'1',grupa:'Grupa 2'}

            let odg1 = await chai.request(server)
                            .post('/student')
                            .set('Content-Type','application/json')
                            .send(JSON.stringify(student1))

            let odg2 = await chai.request(server)
                            .post('/student')
                            .set('Content-Type','application/json')
                            .send(JSON.stringify(student2))
            
            assert.equal(JSON.parse(odg1.text).status,"Kreiran student!", "Nije ispravan odgovor!")
            assert.equal(JSON.parse(odg2.text).status,"Student sa indexom 1 već postoji!", "Nije ispravan odgovor!")

            let studenti = await Student.findAll()
            assert.equal(studenti.length,1, "Nije ispravan broj studenata u bazi!")

            assert.equal(studenti[0].ime,student1.ime, "Neispravno ime prvog studenta")
            assert.equal(studenti[0].prezime,student1.prezime, "Neispravno prezime prvog studenta")
            assert.equal(studenti[0].index,student1.index, "Neispravan index prvog studenta")

            let grupe = await Grupa.findAll()
            assert.equal(grupe.length,1,"Nije ispravan broj grupa u bazi!")
            assert.equal(grupe[0].naziv,student1.grupa,"Nije ispravan naziv grupe prvog studenta")
            assert.equal(studenti[0].GrupaId,grupe[0].id,"Neispravno postavljena grupa prvom studentu")
        })
    });

    describe("Testovi za PUT /student/:index",function() {
        it('Izmjena grupe studentu na postojeću grupu', async function() {
            let student = {ime:'Test',prezime:'Test',index:'1'}
            let novaGrupa = {naziv:'Nova grupa'}

            let staraGrupa = await Grupa.create({naziv:'Stara grupa'})
            novaGrupa = await Grupa.create(novaGrupa)
            student.GrupaId = staraGrupa.id
            await Student.create(student)

            let odg = await chai.request(server)
                            .put('/student/1')
                            .set('Content-Type','application/json')
                            .send(JSON.stringify({grupa: 'Nova grupa'}))
            
            assert.equal(JSON.parse(odg.text).status,"Promjenjena grupa studentu 1", "Nije ispravan odgovor!")
            
            student = await Student.findOne({where: {index:1}})
            assert.equal(student.GrupaId,novaGrupa.id,"Nije promijenjena grupa!")

        });
        it("Izmjena grupe studentu na novu grupu", async function() {
            let student = {ime:'Test',prezime:'Test',index:'1'}

            let staraGrupa = await Grupa.create({naziv:'Stara grupa'})
            student.GrupaId = staraGrupa.id
            await Student.create(student)

            
            let odg = await chai.request(server)
                            .put('/student/1')
                            .set('Content-Type','application/json')
                            .send(JSON.stringify({grupa: 'Nova grupa'}))

            assert.equal(JSON.parse(odg.text).status,"Promjenjena grupa studentu 1", "Nije ispravan odgovor!")

            student = await Student.findOne({where: {index:1}})
            let novaGrupa = await Grupa.findOne({where:{id:student.GrupaId}})
            
            assert.equal(novaGrupa.naziv,'Nova grupa','Nije promijenjena grupa!')

        });

        it("Promjena grupe nepostojećem studentu", async function() {
            let odg = await chai.request(server)
                            .put('/student/1')
                            .set('Content-Type','application/json')
                            .send(JSON.stringify({grupa: 'Nova grupa'}))
            
            assert.equal(JSON.parse(odg.text).status,"Student sa indexom 1 ne postoji", "Nije ispravan odgovor!")

            let grupe = await Grupa.findAll()

            assert.equal(grupe.length,0,"Nije ispravan broj grupa u bazi!")

        });
    });

    describe("Testovi za POST /batch", function() {
        it('Dodavanje validnih studenata sa različitim grupama', async function() {
            let csv = "Test1,Test1,1,Grupa 1\n"
                    + "Test2,Test2,2,Grupa 2\n"
                    + "Test3,Test3,3,Grupa 3\n"

            
            let odg = await chai.request(server)
                    .post('/batch/student')
                    .set('Content-Type','text/plain')
                    .send(csv)
            
            assert.equal(JSON.parse(odg.text).status, "Dodano 3 studenata!","Nije ispravan odgovor!")

            let studenti = await Student.findAll()

            assert.equal(studenti.length,3,'Nije ispravan broj studenata u bazi!')
            

            assert.equal(studenti.map(s => s.ime).includes('Test1'),true,'Neispravno ime prvog studenta!')
            assert.equal(studenti.map(s => s.prezime).includes('Test1'),true,'Neispravno prezime prvog studenta!')
            assert.equal(studenti.map(s => s.index).includes('1'),true,'Neispravan index prvog studenta!')
            
            assert.equal(studenti.map(s => s.ime).includes('Test2'),true,'Neispravno ime drugog studenta!')
            assert.equal(studenti.map(s => s.prezime).includes('Test2'),true,'Neispravno prezime drugog studenta!')
            assert.equal(studenti.map(s => s.index).includes('2'),true,'Neispravan index drugog studenta!')

            assert.equal(studenti.map(s => s.ime).includes('Test3'),true,'Neispravno ime trećeg studenta!')
            assert.equal(studenti.map(s => s.prezime).includes('Test3'),true,'Neispravno prezime trećeg studenta!')
            assert.equal(studenti.map(s => s.index).includes('3'),true,'Neispravan index trećeg studenta!')

            let grupe = await Grupa.findAll()

            assert.equal(grupe.length,3,"Nije ispravan broj grupa u bazi!")

            let grupa1 = await Grupa.findOne({where:{naziv:'Grupa 1'}})
            let student1 = await Student.findOne({where:{index:'1'}})
            
            assert.equal(student1.GrupaId,grupa1.id,"Neispravno postavljena grupa prvom studentu!")

            let grupa2 = await Grupa.findOne({where:{naziv:'Grupa 2'}})
            let student2 = await Student.findOne({where:{index:'2'}})
            
            assert.equal(student2.GrupaId,grupa2.id,"Neispravno postavljena grupa drugom studentu!")

            let grupa3 = await Grupa.findOne({where:{naziv:'Grupa 3'}})
            let student3 = await Student.findOne({where:{index:'3'}})
            
            assert.equal(student3.GrupaId,grupa3.id,"Neispravno postavljena grupa trećem studentu!")
            
        });
        it("Dodavanje validnih studenata s istom postojećom grupom", async function() {
            await Grupa.create({naziv:'Grupa'})

            let csv = "Test1,Test1,1,Grupa\n"
                    + "Test2,Test2,2,Grupa\n"
                    

            
            let odg = await chai.request(server)
                    .post('/batch/student')
                    .set('Content-Type','text/plain')
                    .send(csv)
            
            assert.equal(JSON.parse(odg.text).status, "Dodano 2 studenata!","Nije ispravan odgovor!")

            let studenti = await Student.findAll()

            assert.equal(studenti.length,2,'Nije ispravan broj studenata u bazi!')
            

            assert.equal(studenti.map(s => s.ime).includes('Test1'),true,'Neispravno ime prvog studenta!')
            assert.equal(studenti.map(s => s.prezime).includes('Test1'),true,'Neispravno prezime prvog studenta!')
            assert.equal(studenti.map(s => s.index).includes('1'),true,'Neispravan index prvog studenta!')
            
            assert.equal(studenti.map(s => s.ime).includes('Test2'),true,'Neispravno ime drugog studenta!')
            assert.equal(studenti.map(s => s.prezime).includes('Test2'),true,'Neispravno prezime drugog studenta!')
            assert.equal(studenti.map(s => s.index).includes('2'),true,'Neispravan index drugog studenta!')

            let grupe = await Grupa.findAll()

            assert.equal(grupe.length,1,"Nije ispravan broj grupa u bazi!")

            let student1 = await Student.findOne({where:{index:'1'}})
            let student2 = await Student.findOne({where:{index:'2'}})
            
            assert.equal(student1.GrupaId,grupe[0].id,"Neispravno postavljena grupa prvom studentu!")
            assert.equal(student2.GrupaId,grupe[0].id,"Neispravno postavljena grupa drugom studentu!")

        })
    });
    it("Dodavanje validnih studenata s istom nepostojećom grupom", async function() {
        let csv = "Test1,Test1,1,Grupa\n"
                + "Test2,Test2,2,Grupa\n"
                

        
        let odg = await chai.request(server)
                .post('/batch/student')
                .set('Content-Type','text/plain')
                .send(csv)
        
        assert.equal(JSON.parse(odg.text).status, "Dodano 2 studenata!","Nije ispravan odgovor!")

        let studenti = await Student.findAll()

        assert.equal(studenti.length,2,'Nije ispravan broj studenata u bazi!')
        

        assert.equal(studenti.map(s => s.ime).includes('Test1'),true,'Neispravno ime prvog studenta!')
        assert.equal(studenti.map(s => s.prezime).includes('Test1'),true,'Neispravno prezime prvog studenta!')
        assert.equal(studenti.map(s => s.index).includes('1'),true,'Neispravan index prvog studenta!')
        
        assert.equal(studenti.map(s => s.ime).includes('Test2'),true,'Neispravno ime drugog studenta!')
        assert.equal(studenti.map(s => s.prezime).includes('Test2'),true,'Neispravno prezime drugog studenta!')
        assert.equal(studenti.map(s => s.index).includes('2'),true,'Neispravan index drugog studenta!')

        let grupe = await Grupa.findAll()

        assert.equal(grupe.length,1,"Nije ispravan broj grupa u bazi!")

        let student1 = await Student.findOne({where:{index:'1'}})
        let student2 = await Student.findOne({where:{index:'2'}})
        
        assert.equal(student1.GrupaId,grupe[0].id,"Neispravno postavljena grupa prvom studentu!")
        assert.equal(student2.GrupaId,grupe[0].id,"Neispravno postavljena grupa drugom studentu!")

    });
    
    it("Dodavanje validnih i nevalidnih studenata na praznu bazu", async function() {
        let csv = "Test1,Test1,1,Grupa 1\n"
                + "niko,niko,1,niko\n"
                + "niko,niko,1,Grupa 1\n"
                +"Test2,Test2,2,Grupa 2\n"
                +"niko,niko,2,niko\n"
                +"niko,niko,1,niko\n"
                +"niko,niko,2,Grupa 2\n"

        let odg = await chai.request(server)
                .post('/batch/student')
                .set('Content-Type','text/plain')
                .send(csv)
        
        assert.equal(JSON.parse(odg.text).status, "Dodano 2 studenata, a studenti 1,1,2,1,2 već postoje!",
                "Nije ispravan odgovor!")

        let studenti = await Student.findAll()

        assert.equal(studenti.length,2,'Nije ispravan broj studenata u bazi!')
        

        assert.equal(studenti.map(s => s.ime).includes('Test1'),true,'Neispravno ime prvog studenta!')
        assert.equal(studenti.map(s => s.prezime).includes('Test1'),true,'Neispravno prezime prvog studenta!')
        assert.equal(studenti.map(s => s.index).includes('1'),true,'Neispravan index prvog studenta!')
        
        assert.equal(studenti.map(s => s.ime).includes('Test2'),true,'Neispravno ime drugog studenta!')
        assert.equal(studenti.map(s => s.prezime).includes('Test2'),true,'Neispravno prezime drugog studenta!')
        assert.equal(studenti.map(s => s.index).includes('2'),true,'Neispravan index drugog studenta!')

        let grupe = await Grupa.findAll()

        assert.equal(grupe.length,2,"Nije ispravan broj grupa u bazi!")

        let grupa1 = await Grupa.findOne({where:{naziv:'Grupa 1'}})
        let student1 = await Student.findOne({where:{index:'1'}})
            
        assert.equal(student1.GrupaId,grupa1.id,"Neispravno postavljena grupa prvom studentu!")

        let grupa2 = await Grupa.findOne({where:{naziv:'Grupa 2'}})
        let student2 = await Student.findOne({where:{index:'2'}})
            
        assert.equal(student2.GrupaId,grupa2.id,"Neispravno postavljena grupa drugom studentu!")
    });
    it("Uzastopno dodavanje studenata, validnih i nevalidnih", async function() {
        let sviValidni ="Test1,Test1,1,Grupa 1\n"
                        +"Test2,Test2,2,Grupa 1"

        let validniNevalidni = "niko,niko,1,niko\n"
                            + "niko,niko,2,niko\n"
                            + "niko,niko,2,niko\n"
                            + "Test3,Test3,3,Grupa 1\n"
                            + "Test4,Test4,4,Grupa 2\n"
                            + "niko,niko,4,niko\n"
                            + "niko,niko,3,niko\n"
                            + "niko,niko,4,niko\n"
                            + "niko,niko,3,niko\n"

        let odg1 = await chai.request(server)
                    .post('/batch/student')
                    .set('Content-Type','text/plain')
                    .send(sviValidni)
        
        assert.equal(JSON.parse(odg1.text).status, "Dodano 2 studenata!", "Nije ispravan odgovor!")

        let odg2 = await chai.request(server)
                    .post('/batch/student')
                    .set('Content-Type','text/plain')
                    .send(validniNevalidni)

        assert.equal(JSON.parse(odg2.text).status, "Dodano 2 studenata, a studenti 1,2,2,4,3,4,3 već postoje!", 
                "Nije ispravan odgovor!")

        let studenti = await Student.findAll()

        assert.equal(studenti.length,4,"Nije ispravan broj studenata u bazi!")

        assert.equal(studenti.map(s => s.ime).includes('Test1'),true,'Neispravno ime prvog studenta!')
        assert.equal(studenti.map(s => s.prezime).includes('Test1'),true,'Neispravno prezime prvog studenta!')
        assert.equal(studenti.map(s => s.index).includes('1'),true,'Neispravan index prvog studenta!')
        
        assert.equal(studenti.map(s => s.ime).includes('Test2'),true,'Neispravno ime drugog studenta!')
        assert.equal(studenti.map(s => s.prezime).includes('Test2'),true,'Neispravno prezime drugog studenta!')
        assert.equal(studenti.map(s => s.index).includes('2'),true,'Neispravan index drugog studenta!')

        assert.equal(studenti.map(s => s.ime).includes('Test3'),true,'Neispravno ime trećeg studenta!')
        assert.equal(studenti.map(s => s.prezime).includes('Test3'),true,'Neispravno prezime trećeg studenta!')
        assert.equal(studenti.map(s => s.index).includes('3'),true,'Neispravan index trećeg studenta!')
        
        assert.equal(studenti.map(s => s.ime).includes('Test4'),true,'Neispravno ime četvrtog studenta!')
        assert.equal(studenti.map(s => s.prezime).includes('Test4'),true,'Neispravno prezime četvrtog studenta!')
        assert.equal(studenti.map(s => s.index).includes('4'),true,'Neispravan index četvrtog studenta!')

        let grupe = await Grupa.findAll()

        assert.equal(grupe.length,2,"Nije ispravan broj grupa u bazi!")

        let grupa1 = await Grupa.findOne({where:{naziv:'Grupa 1'}})
        let grupa2 = await Grupa.findOne({where:{naziv:'Grupa 2'}})

        let student1 = await Student.findOne({where:{index:'1'}})
        let student2 = await Student.findOne({where:{index:'2'}})
        let student3 = await Student.findOne({where:{index:'3'}})
        let student4 = await Student.findOne({where:{index:'4'}})    

        assert.equal(student1.GrupaId,grupa1.id,"Neispravno postavljena grupa prvom studentu!")
        assert.equal(student2.GrupaId,grupa1.id,"Neispravno postavljena grupa drugom studentu!")
        assert.equal(student3.GrupaId,grupa1.id,"Neispravno postavljena grupa trećem studentu!")
        assert.equal(student4.GrupaId,grupa2.id,"Neispravno postavljena grupa četvrtom studentu!")

    });

});

