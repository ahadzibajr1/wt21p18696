let assert = chai.assert

   describe('TestoviParser', function() {
       describe('dajTacnost()', function() {
           it('Svi testovi prolaze', function() {
               var izvjestaj = `{
                "stats": {
                    "suites": 1,
                    "tests": 1,
                    "passes": 1,
                    "pending": 0,
                    "failures": 0,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 5
                },
                "tests": [
                    {
                        "title": "test koji prolazi",
                        "fullTitle": "test koji prolazi",
                        "file": null,
                        "duration": 5,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [],
                "passes": [
                    {
                        "title": "test koji prolazi",
                        "fullTitle": "test koji prolazi",
                        "file": null,
                        "duration": 5,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            }`
               var tacnostObjekat = TestoviParser.dajTacnost(izvjestaj)
               assert.equal(tacnostObjekat.tacnost, "100%", "Tacnost treba biti 100%")
               assert.equal(tacnostObjekat.greske.length, 0, "Broj gresaka treba biti 0")
           
            });
            it('Svi testovi padaju', function() {
                var izvjestaj  = `{
                    "stats": {
                        "suites": 1,
                        "tests": 2,
                        "passes": 0,
                        "pending": 0,
                        "failures": 2,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 5
                    },
                    "tests": [
                        {
                            "title": "test koji pada",
                            "fullTitle": "test koji pada",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "drugi test koji pada",
                            "fullTitle": "drugi test koji pada",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [
                        {
                            "title": "test koji pada",
                            "fullTitle": "test koji pada",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "drugi test koji pada",
                            "fullTitle": "drugi test koji pada",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "passes": []
                }`
                var tacnostObjekat = TestoviParser.dajTacnost(izvjestaj)
                assert.equal(tacnostObjekat.tacnost, "0%", "Tacnost treba biti 0%")
                assert.equal(tacnostObjekat.greske.length, 2, "Broj gresaka treba biti 2")
                assert.equal(tacnostObjekat.greske[0], "test koji pada", "Treba da vrati naziv prvog testa" )
                assert.equal(tacnostObjekat.greske[1], "drugi test koji pada", "Treba da vrati naziv drugog testa")
            });
            it('Jedan test prolazi, drugi pada', function() {
                var izvjestaj  = `{
                    "stats": {
                        "suites": 2,
                        "tests": 2,
                        "passes": 1,
                        "pending": 0,
                        "failures": 1,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 5
                    },
                    "tests": [
                        {
                            "title": "test koji prolazi",
                            "fullTitle": "test koji prolazi",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test koji pada",
                            "fullTitle": "test koji pada",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [
                        {
                            "title": "test koji pada",
                            "fullTitle": "test koji pada",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "passes": [
                        {
                            "title": "test koji prolazi",
                            "fullTitle": "test koji prolazi",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ]
                }`
                var tacnostObjekat = TestoviParser.dajTacnost(izvjestaj)
                assert.equal(tacnostObjekat.tacnost, "50%", "Tacnost treba biti 50%")
                assert.equal(tacnostObjekat.greske.length, 1, "Broj gresaka treba biti 1")
                assert.equal(tacnostObjekat.greske[0], "test koji pada", "Treba da vrati korektan naziv testa" )
            });
            it('Dva testa prolaze, jedan pada, testiranje zaokruzivanja na decimale', function() {
                var izvjestaj  = `{
                    "stats": {
                        "suites": 3,
                        "tests": 3,
                        "passes": 2,
                        "pending": 0,
                        "failures": 1,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 7
                    },
                    "tests": [
                        {
                            "title": "prvi test koji prolazi",
                            "fullTitle": "prvi test koji prolazi",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "drugi test koji prolazi",
                            "fullTitle": "drugi test koji prolazi",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test koji pada",
                            "fullTitle": "test koji pada",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [
                        {
                            "title": "test koji pada",
                            "fullTitle": "test koji pada",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "passes": [
                        {
                            "title": "prvi test koji prolazi",
                            "fullTitle": "prvi test koji prolazi",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "drugi test koji prolazi",
                            "fullTitle": "drugi test koji prolazi",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ]
                }`
                var tacnostObjekat = TestoviParser.dajTacnost(izvjestaj)
                assert.equal(tacnostObjekat.tacnost, "66.7%", "Tacnost treba biti 66.7%")
                assert.equal(tacnostObjekat.greske.length, 1, "Broj gresaka treba biti 1")
                assert.equal(tacnostObjekat.greske[0], "test koji pada", "Treba da vrati korektan naziv testa" )
            });
            it('Testovi se ne mogu pokrenuti', function() {
                var izvjestaj  = 'neodgovarajuci format'
                var tacnostObjekat = TestoviParser.dajTacnost(izvjestaj)
                assert.equal(tacnostObjekat.tacnost, "0%", "Tacnost treba biti 0%")
                assert.equal(tacnostObjekat.greske.length, 1, "Broj gresaka treba biti 1")
                assert.equal(tacnostObjekat.greske[0], "Testovi se ne mogu izvršiti", "Treba da vrati poruku da se testovi ne mogu izvršiti" )
            });
        });
    });
   
