var VjezbeAjax = (function() {
    var kliknuto = []
    var polja = []
    var dodajInputPolja = function(DOMelementDIVauFormi,brojVjezbi) {

        if(parseFloat(brojVjezbi)-parseInt(brojVjezbi) != 0) {
            console.log("Pogrešan broj vježbi!")
            return
        }

        if (brojVjezbi>15 || brojVjezbi<1) {
            console.log("Pogrešan broj vježbi!")
            return
        }

        DOMelementDIVauFormi.innerHTML=""

        for(let i = 0; i<brojVjezbi; i++)
            {
                var inputPolje = document.createElement("input")
                var labela = document.createElement("label")
                var prelaz = document.createElement("br")

                inputPolje.setAttribute("type","number")
                var id = "z" + i;
                inputPolje.setAttribute("id",id)
                inputPolje.setAttribute("name",id)
                inputPolje.setAttribute("value",4)
                labela.setAttribute("name",id)
                labela.innerHTML = "Broj zadataka u vježbi "+ (i+1) + ": "
                labela.setAttribute("style", "margin-top: 10px; margin-right: 10px")
                inputPolje.setAttribute("style","margin-top: 5px; padding: 5px")
                inputPolje.setAttribute("class","polje")
                
                DOMelementDIVauFormi.appendChild(labela)
                DOMelementDIVauFormi.appendChild(inputPolje)
                DOMelementDIVauFormi.appendChild(prelaz)
                
            }
    }

    var posaljiPodatke = function(vjezbaObjekat,callbackFja) {

        //provjera podataka koji se šalju na klijentskoj strani
        var greska = "Pogrešan parametar "
        if((parseFloat(vjezbaObjekat.brojVjezbi)-parseInt(vjezbaObjekat.brojVjezbi))!=0  
            || vjezbaObjekat.brojVjezbi <1 || vjezbaObjekat.brojVjezbi>15)
            greska+="brojVjezbi"

        if(vjezbaObjekat.brojVjezbi != vjezbaObjekat.brojZadataka.length) {
            if(greska.length >= 20)
                greska+=",brojZadataka"
            else 
                greska+="brojZadataka"
        }

        var brojac = 0
        vjezbaObjekat.brojZadataka.forEach(z => {if((parseFloat(z)-parseInt(z))!=0 ||  z<0 || z>10){
                if(greska.length >= 20)
                    greska+=",z" + brojac
                else
                    greska+="z" + brojac
            }
                brojac++
            })

        if (greska.length>=20) {
            callbackFja(greska,null)
            console.log(greska)
            return
        }

        //ako je sve ok, posalji zahtjev
        const ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && JSON.parse(ajax.responseText).status != "error") {
                var str = ajax.responseText
                var response = JSON.parse(str)
                var err = null
                callbackFja(err,response)
                console.log(response)
            }
            if (ajax.readyState == 4 && JSON.parse(ajax.responseText).status == "error") {
                var err = JSON.parse(ajax.responseText).data
                var response = null
                callbackFja(err,response)
                console.log(err)
            }
            
         }
        ajax.open("POST", "http://localhost:3000/vjezbe",true)
        ajax.setRequestHeader("Content-Type", "application/json")
        ajax.send(JSON.stringify(vjezbaObjekat))
    }

    var dohvatiPodatke = function(callbackFja) {
        const ajax = new XMLHttpRequest();

        ajax.onreadystatechange = function() {

            if (ajax.readyState == 4 && ajax.status == 200) {

                //provjera pristiglih podataka s klijentske strane

                let greska ="Pogrešan parametar "
                let brojac = 0
                var response = JSON.parse(ajax.responseText)
                if(parseFloat((response.brojVjezbi)-parseInt(response.brojVjezbi))!=0 
                    || response.brojVjezbi <1 || response.brojVjezbi>15) {
                    greska +="brojVjezbi"
                }
                

                response.brojZadataka.forEach(z => {if((parseFloat(z)-parseInt(z))!=0 || z<0 || z>10){
                    if(greska.length>=20)
                        greska +=",z" + brojac
                    else
                        greska +="z" + brojac
                    }
                    brojac++
                })

                if( response.brojVjezbi != response.brojZadataka.length){
                    if(greska.length>=20)
                        greska+=",brojZadataka"
                    else
                        greska+="brojZadataka"
                }

                if(greska.length>=20) {
                    console.log(greska)
                    callbackFja(greska,null)
                    return
                }

                //ako je sve ok, proslijedi odgovor
                var err = null
                callbackFja(err,response)
            }
            if (ajax.readyState == 4 && ajax.status != 200) {
                var err = ajax.statusText
                var response = null
                callbackFja(err,response)
            }
            
         }
        ajax.open("GET","http://localhost:3000/vjezbe",true)
        ajax.send()
    }

    var iscrtajVjezbe = function(divDOMelement,vjezbeObjekat) {

        if(vjezbeObjekat.brojVjezbi>15 || vjezbeObjekat.brojVjezbi<1 || 
            (parseFloat(vjezbeObjekat.brojVjezbi)-parseInt(vjezbeObjekat.brojVjezbi))!=0) {
            console.log("Pogrešan broj vježbi!")
            return
        }

        if(vjezbeObjekat.brojVjezbi != vjezbeObjekat.brojZadataka.length) {
            console.log("Pogrešan broj zadataka!")
            return
        }

        for(let i=0; i<vjezbeObjekat.brojVjezbi; i++) {
            let vjezba = document.createElement("div")
            let dugmeVjezba = document.createElement("button")
            
            var nazivVjezbe = "VJEŽBA " + (i+1)
            dugmeVjezba.setAttribute("value",nazivVjezbe)
            dugmeVjezba.addEventListener("click", function() 
            {iscrtajZadatke(vjezba,vjezbeObjekat.brojZadataka[i]);})
            dugmeVjezba.innerText = nazivVjezbe
            dugmeVjezba.setAttribute("class","vjezba-btn")

            
            vjezba.appendChild(dugmeVjezba)
            divDOMelement.appendChild(vjezba)

        }
    }

    var iscrtajZadatke = function(vjezbaDOMelement,brojZadataka) {

        if(brojZadataka>10 || brojZadataka<0 || (parseFloat(brojZadataka)-parseInt(brojZadataka))!=0) {
            console.log("Pogrešan broj zadataka!")
            return
        }

        
        //provjera da li je vec kliknuto na vjezbu
        if(!kliknuto.includes(vjezbaDOMelement)) {
            //pamtimo da je kliknuto na vjezbu
            kliknuto.push(vjezbaDOMelement)
            var poljeZadaci = document.createElement("div")
            poljeZadaci.setAttribute("class", "zadaci")
            for(let i=0; i<brojZadataka; i++){
                var nazivZadatka = "Zadatak " + (i+1)
                var dugmeZadatak = document.createElement("Button")
                dugmeZadatak.setAttribute("class","zadatak-btn")
                dugmeZadatak.setAttribute("value",nazivZadatka)
                dugmeZadatak.innerText = nazivZadatka
                
               
                poljeZadaci.appendChild(dugmeZadatak)
            }
                vjezbaDOMelement.appendChild(poljeZadaci)
                //pamtimo polje koje je otvoreno
                polja.push(poljeZadaci)
                var display ="block"
            
            //ako je vec kliknuto na vjezbu    
        } else {
            var index = kliknuto.indexOf(vjezbaDOMelement)
            var poljeZadaci = polja[index]
            //ako su skriveni zadaci,stavljamo da ih treba prikazati
            if (polja[index].style.display == "none")
                 display = "block"
            //ako zadaci nisu skriveni,treba ih sakriti
            else
                display = "none"

            
        }


        //skrivamo sva polja
        for (let i = 0; i< polja.length; i++) {
            polja[i].style.display = "none"
        }

        //postavljamo da je polje na koje je sad kliknuto vidljivo ili nevidljivo
        poljeZadaci.setAttribute("style", "display: "+display)
        
    }


    return {dodajInputPolja: dodajInputPolja,
            posaljiPodatke: posaljiPodatke,
            dohvatiPodatke: dohvatiPodatke,
            iscrtajVjezbe: iscrtajVjezbe,
            iscrtajZadatke: iscrtajZadatke}
}());

