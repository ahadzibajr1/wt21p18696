var StudentAjax = (function() {

    var dodajStudenta = function(student, callback) {
        const ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4) {
                var str = ajax.responseText
                callback(str)
            }
            
         }

        ajax.open("POST", "http://localhost:3000/student",true)
        ajax.setRequestHeader("Content-Type", "application/json")
        ajax.send(JSON.stringify(student))

    }

    var postaviGrupu = function(index,grupa, callback) {
        const ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4) {
                var str = ajax.responseText
                callback(str)
            }
            
         }

        ajax.open("PUT", "http://localhost:3000/student/"+index,true)
        ajax.setRequestHeader("Content-Type", "application/json")
        ajax.send(JSON.stringify({grupa: grupa}))

    }

    var dodajBatch = function(csvStudenti, callback) {
        const ajax = new XMLHttpRequest()

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4) {
                var str = ajax.responseText
                callback(str)
            }
            
         }

        ajax.open("POST", "http://localhost:3000/batch/student")
        ajax.setRequestHeader("Content-Type","text/plain")
        ajax.send(csvStudenti)
    }


    return {
        dodajStudenta: dodajStudenta,
        postaviGrupu: postaviGrupu,
        dodajBatch: dodajBatch
    }
}());