let assert = chai.assert

   describe('TestoviParser', function() {
       describe('TestoviParser.porediRezultate()', function() {
           it('Slanje identicnih izvjestaja za poredenje', function() {
               var izvjestaj = `{
                "stats": {
                    "suites": 1,
                    "tests": 1,
                    "passes": 1,
                    "pending": 0,
                    "failures": 0,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 5
                },
                "tests": [
                    {
                        "title": "test koji prolazi",
                        "fullTitle": "test koji prolazi",
                        "file": null,
                        "duration": 5,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [],
                "passes": [
                    {
                        "title": "test koji prolazi",
                        "fullTitle": "test koji prolazi",
                        "file": null,
                        "duration": 5,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            }`
               var poredbaObjekat = TestoviParser.porediRezultate(izvjestaj, izvjestaj)
               assert.equal(poredbaObjekat.promjena, "100%", "Promjena treba biti 100%")
               assert.equal(poredbaObjekat.greske.length, 0, "Broj gresaka treba biti 0")
            });
            it('Poredenje izvjestaja s identicnim testovima', function() {
                var izvjestaj1 = `{
                    "stats": {
                        "suites": 3,
                        "tests": 3,
                        "passes": 1,
                        "pending": 0,
                        "failures": 2,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 7
                    },
                    "tests": [
                        {
                            "title": "prvi test",
                            "fullTitle": "prvi test",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "drugi test",
                            "fullTitle": "drugi test",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "treci test",
                            "fullTitle": "treci test",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [
                        {
                            "title": "treci test",
                            "fullTitle": "treci test",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "prvi test",
                            "fullTitle": "prvi test",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "passes": [
                        {
                            "title": "drugi test",
                            "fullTitle": "drugi test",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ]
                }`
                var izvjestaj2 = `{
                    "stats": {
                        "suites": 3,
                        "tests": 3,
                        "passes": 2,
                        "pending": 0,
                        "failures": 1,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 7
                    },
                    "tests": [
                        {
                            "title": "treci test",
                            "fullTitle": "treci test",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "drugi test",
                            "fullTitle": "drugi test",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "prvi test",
                            "fullTitle": "prvi test",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }  
                    ],
                    "pending": [],
                    "failures": [
                        {
                            "title": "treci test",
                            "fullTitle": "treci test",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "passes": [
                        {
                            "title": "prvi test",
                            "fullTitle": "prvi test",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "drugi test",
                            "fullTitle": "drugi test",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ]
                }`
                var poredbaObjekat = TestoviParser.porediRezultate(izvjestaj1, izvjestaj2)
                assert.equal(poredbaObjekat.promjena, "66.7%", "Promjena treba biti 66.6%, sto je tacnost u rezultat2")
                assert.equal(poredbaObjekat.greske.length, 1, "Broj gresaka treba biti 1, jer toliko je greska u rezultat2")
                assert.equal(poredbaObjekat.greske[0], "treci test", "Treci test pada u rezultat2")
             });
             it('Razliciti testovi, 1 zajednicka greska', function() {
                var izvjestaj1 = `{
                    "stats": {
                        "suites": 2,
                        "tests": 2,
                        "passes": 0,
                        "pending": 0,
                        "failures": 2,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 7
                    },
                    "tests": [
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 2",
                            "fullTitle": "test 2",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 2",
                            "fullTitle": "test 2",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                
                    ],
                    "passes": []
                }`
                var izvjestaj2 = `{
                    "stats": {
                        "suites": 2,
                        "tests": 2,
                        "passes": 0,
                        "pending": 0,
                        "failures": 2,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 7
                    },
                    "tests": [
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 3",
                            "fullTitle": "test 3",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [
                        {
                            "title": "test 3",
                            "fullTitle": "test 3",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                
                    ],
                    "passes": []
                }`
                var poredbaObjekat = TestoviParser.porediRezultate(izvjestaj1, izvjestaj2)
                assert.equal(poredbaObjekat.promjena, "100%", "Promjena treba biti jednaka 100%")
                assert.equal(poredbaObjekat.greske.length, 3, "Broj gresaka treba da bude 3, jedna iz rezultat1 koja nije testirana u rezultat2, te dvije iz rezultat2")
                assert.equal(poredbaObjekat.greske[0], "test 2", "Prva greska je iz rezultat1")
                assert.equal(poredbaObjekat.greske[1], "test 1", "Druga greska je iz rezultat2")
                assert.equal(poredbaObjekat.greske[2], "test 3", "Treca greska je iz rezultat2")
             });
             it('Razliciti testovi, svi testovi iz rezultata1 prolaze u rezultat2', function() {
                var izvjestaj1 = `{
                    "stats": {
                        "suites": 2,
                        "tests": 3,
                        "passes": 0,
                        "pending": 0,
                        "failures": 3,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 7
                    },
                    "tests": [
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 2",
                            "fullTitle": "test 2",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 3",
                            "fullTitle": "test 3",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 3",
                            "fullTitle": "test 3",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 2",
                            "fullTitle": "test 2",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                
                    ],
                    "passes": []
                }`
                var izvjestaj2 = `{
                    "stats": {
                        "suites": 2,
                        "tests": 4,
                        "passes": 4,
                        "pending": 0,
                        "failures": 0,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 7
                    },
                    "tests": [
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 2",
                            "fullTitle": "test 2",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 3",
                            "fullTitle": "test 3",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 4",
                            "fullTitle": "test 4",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [],
                    "passes": [
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 2",
                            "fullTitle": "test 2",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 3",
                            "fullTitle": "test 3",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 4",
                            "fullTitle": "test 4",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ]
                }`
                var poredbaObjekat = TestoviParser.porediRezultate(izvjestaj1, izvjestaj2)
                assert.equal(poredbaObjekat.promjena, "0%", "Promjena treba da bude 0%")
                assert.equal(poredbaObjekat.greske.length, 0, "Sve greske su popravljene u izvjestaj2")
            });
            it('Razliciti testovi, 2 testa pokvarena u izvjestaj2', function() {
                var izvjestaj1 = `{
                    "stats": {
                        "suites": 2,
                        "tests": 3,
                        "passes": 3,
                        "pending": 0,
                        "failures": 0,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 7
                    },
                    "tests": [
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 2",
                            "fullTitle": "test 2",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 3",
                            "fullTitle": "test 3",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [],
                    "passes": [
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 3",
                            "fullTitle": "test 3",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 2",
                            "fullTitle": "test 2",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                
                    ]
                }`
                var izvjestaj2 = `{
                    "stats": {
                        "suites": 2,
                        "tests": 2,
                        "passes": 0,
                        "pending": 0,
                        "failures": 2,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 7
                    },
                    "tests": [
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 2",
                            "fullTitle": "test 2",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [
                        {
                            "title": "test 2",
                            "fullTitle": "test 2",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "passes": []
                }`
                var poredbaObjekat = TestoviParser.porediRezultate(izvjestaj1, izvjestaj2)
                assert.equal(poredbaObjekat.promjena, "100%", "Promjena treba da bude 100%")
                assert.equal(poredbaObjekat.greske.length, 2, "Sve greske su iz izvjestaj2")
                assert.equal(poredbaObjekat.greske[0], "test 1", "Test 1 pada u izvjestaj2")
                assert.equal(poredbaObjekat.greske[1], "test 2", "Test 2 pada u izvjestaj2")

            });
            it('Potpuno razliciti testovi', function() {
                var izvjestaj1 = `{
                    "stats": {
                        "suites": 2,
                        "tests": 2,
                        "passes": 0,
                        "pending": 0,
                        "failures": 2,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 7
                    },
                    "tests": [
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 2",
                            "fullTitle": "test 2",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [
                        {
                            "title": "test 1",
                            "fullTitle": "test 1",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 2",
                            "fullTitle": "test 2",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                
                    ],
                    "passes": []
                }`
                var izvjestaj2 = `{
                    "stats": {
                        "suites": 2,
                        "tests": 2,
                        "passes": 1,
                        "pending": 0,
                        "failures": 1,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 7
                    },
                    "tests": [
                        {
                            "title": "test 3",
                            "fullTitle": "test 3",
                            "file": null,
                            "duration": 3,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "test 4",
                            "fullTitle": "test 4",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [
                        {
                            "title": "test 4",
                            "fullTitle": "test 4",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                
                    ],
                    "passes": [
                        {
                            "title": "test 3",
                            "fullTitle": "test 3",
                            "file": null,
                            "duration": 2,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ]
                }`
                var poredbaObjekat = TestoviParser.porediRezultate(izvjestaj1, izvjestaj2)
                assert.equal(poredbaObjekat.promjena, "75%", "Promjena treba da bude 75%")
                assert.equal(poredbaObjekat.greske.length, 3, "Kako su potpuno razliciti testovi u izvjestajima, sve greske iz oba izvjestaja trebaju biti vracene")
                assert.equal(poredbaObjekat.greske[0], "test 1", "Test 1 pada u izvjestaj1")
                assert.equal(poredbaObjekat.greske[1], "test 2", "Test 2 pada u izvjestaj1")
                assert.equal(poredbaObjekat.greske[2], "test 4", "Test 4 pada u izvjestaj2")
            });
            it('Nepodrzan format izvjestaja', function() {
                var izvjestaj = `{"stats": {
                                "tests" : 2,
                                "failures" : 1,
                                "passes" : 1
                }}`
                var poredbaObjekat = TestoviParser.porediRezultate(izvjestaj, izvjestaj)
                assert.equal(poredbaObjekat.promjena, "0%", "Promjena treba biti 0%")
                assert.equal(poredbaObjekat.greske[0], "Testovi se ne mogu izvršiti", "Trebamo dobiti poruku da se testovi ne mogu izvrsiti")                
            });
        });
   });