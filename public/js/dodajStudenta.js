window.onload = function() {
    var dugme = document.getElementById("btnSlanje")
    dugme.addEventListener("click", function(e) {
        e.preventDefault()
        var ime = document.getElementById("ime").value
        var prezime = document.getElementById("prezime").value
        var index = document.getElementById("index").value
        var grupa = document.getElementById("grupa").value

        document.getElementById("ajaxstatus").innerText =""
        if(ime.trim().length==0) {
            console.log("Morate unijeti ime!")
            document.getElementById("ajaxstatus").innerText = "Morate unijeti ime!"
            return
        }

        if(prezime.trim().length==0) {
            console.log("Morate unijeti prezime!")
            document.getElementById("ajaxstatus").innerText += "\nMorate unijeti prezime!"
            return
        }
        if(index.trim().length==0) {
            console.log("Morate unijeti index!")
            document.getElementById("ajaxstatus").innerText += "\nMorate unijeti index!"
            return
        }
        if(grupa.trim().length==0) {
            console.log("Morate unijeti grupu!")
            document.getElementById("ajaxstatus").innerText += "\nMorate unijeti grupu!"
            return
        }

        let student = new Object()
        student.ime = ime
        student.prezime = prezime
        student.index = index
        student.grupa = grupa
        
        StudentAjax.dodajStudenta(student, (odg)=> {
            document.getElementById("ajaxstatus").innerText =JSON.parse(odg).status.toString()
        })
    })

}