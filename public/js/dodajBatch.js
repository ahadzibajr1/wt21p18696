

window.onload = function() {
    var dugme = document.getElementById("btnSlanje")
    dugme.addEventListener("click", function(e) {
        
        var unos = document.getElementById("csv").value
        
        var redoviCSV = unos.split(/\n/)
        
         for (let i=0; i<redoviCSV.length; i++) {
            
            if (redoviCSV[i].split(",").length!= 4) {
                console.log("Neispravan format!")
                document.getElementById("ajaxstatus").innerText = "Neispravan format!"
                return
            }
         }
        
        
        StudentAjax.dodajBatch(unos,function(str) {
            document.getElementById("ajaxstatus").innerText =JSON.parse(str).status.toString()
        })
    })

}