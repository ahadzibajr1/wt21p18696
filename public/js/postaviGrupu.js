window.onload = function() {
    var dugme = document.getElementById("btnPotvrda")
    dugme.addEventListener("click", function(e) {
        e.preventDefault()
        var index = document.getElementById("index").value
        var grupa = document.getElementById("grupa").value

        if(grupa.trim().length==0) {
            console.log("Morate unijeti grupu!")
            return
        }

        
        StudentAjax.postaviGrupu(index,grupa, (odg)=> {
            document.getElementById("ajaxstatus").innerText = JSON.parse(odg).status.toString()
        })
    })

}