let assert = chai.assert
chai.should()


describe ('VjezbeAjax', function() {
    beforeEach(function() {
        this.xhr = sinon.useFakeXMLHttpRequest();
     
        this.requests = [];
        this.xhr.onCreate = function(xhr) {
          this.requests.push(xhr);
        }.bind(this);
      });
     
      afterEach(function() {
        this.xhr.restore();
      });
    describe ('dodajInputPolja()', function() {
        it ('Dodavanje polja u prazan div', function() {
            var div = document.getElementById('divInput')
            div.innerHTML=""
            VjezbeAjax.dodajInputPolja(div,3)
            var input1 = document.getElementById('z0')
            var input2 = document.getElementById('z1')
            var input3 = document.getElementById('z2')

            assert.equal(input1.value, 4, "Default vrijednost nije 4 ili input polje ne postoji")
            assert.equal(input2.value, 4, "Default vrijednost nije 4 ili input polje ne postoji")
            assert.equal(input3.value, 4, "Default vrijednost nije 4 ili input polje ne postoji")
        });
        it ('Dodavanje polja u div koji nije prazan', function() {
            var div = document.getElementById('divInput')
            div.innerHTML = ""
            VjezbeAjax.dodajInputPolja(div,3)
            VjezbeAjax.dodajInputPolja(div,2)

            var brojInputPolja = 0
            for(let i=0; i<div.childNodes.length; i++)
                if(div.childNodes[i].id.includes("z"))
                    brojInputPolja++
        
            assert.equal(brojInputPolja, 2, "U divu nisu 2 input polja")
        });
        it ('Dodavanje 0 polja', function() {
            var div = document.getElementById('divInput')
            div.innerHTML = ""
            VjezbeAjax.dodajInputPolja(div,0)

            var brojInputPolja = 0
            for(let i=0; i<div.childNodes.length; i++)
                if(div.childNodes[i].id.includes("z"))
                    brojInputPolja++
        
            assert.equal(brojInputPolja, 0, "U divu ima input polja")
        });
        it ('Dodavanje negativnog broja polja', function() {
            var div = document.getElementById('divInput')
            div.innerHTML = ""
            VjezbeAjax.dodajInputPolja(div,-5)

            var brojInputPolja = 0
            for(let i=0; i<div.childNodes.length; i++)
                if(div.childNodes[i].id.includes("z"))
                    brojInputPolja++
        
            assert.equal(brojInputPolja, 0, "U divu ima input polja")
        });
        it ('Dodavanje više od 15 polja', function() {
            var div = document.getElementById('divInput')
            div.innerHTML = ""
            VjezbeAjax.dodajInputPolja(div,30)

            var brojInputPolja = 0
            for(let i=0; i<div.childNodes.length; i++)
                if(div.childNodes[i].id.includes("z"))
                    brojInputPolja++
        
            assert.equal(brojInputPolja, 0, "U divu ima input polja")
        });
        it ('Dodavanje decimalnog broja polja', function() {
            var div = document.getElementById('divInput')
            div.innerHTML = ""
            VjezbeAjax.dodajInputPolja(div,2.5)

            var brojInputPolja = 0
            for(let i=0; i<div.childNodes.length; i++)
                if(div.childNodes[i].id.includes("z"))
                    brojInputPolja++
        
            assert.equal(brojInputPolja, 0, "U divu ima input polja")
        });
    });
    describe ('iscrtajVjezbe()', function() {
        it('Iscrtavanje validnog broja vježbi', function() {
            var div = document.getElementById('divInput')
            div.innerHTML=""
            var vjezbeObjekat = new Object()
            vjezbeObjekat.brojVjezbi = 3
            vjezbeObjekat.brojZadataka = [2,1,2]
            VjezbeAjax.iscrtajVjezbe(div,vjezbeObjekat)
            assert.equal(div.childElementCount, 3)
        });
        it('Iscrtavanje negativnog broja vježbi', function() {
            var div = document.getElementById('divInput')
            div.innerHTML=""
            var vjezbeObjekat = new Object()
            vjezbeObjekat.brojVjezbi = -1
            vjezbeObjekat.brojZadataka = [2,1,2]
            VjezbeAjax.iscrtajVjezbe(div,vjezbeObjekat)
            assert.equal(div.childElementCount, 0)
        });
        it('Iscrtavanje nula vježbi', function() {
            var div = document.getElementById('divInput')
            div.innerHTML=""
            var vjezbeObjekat = new Object()
            vjezbeObjekat.brojVjezbi = 0
            vjezbeObjekat.brojZadataka = []
            VjezbeAjax.iscrtajVjezbe(div,vjezbeObjekat)
            assert.equal(div.childElementCount, 0)
        });
        it('Iscrtavanje više od 15 vježbi', function() {
            var div = document.getElementById('divInput')
            div.innerHTML=""
            var vjezbeObjekat = new Object()
            vjezbeObjekat.brojVjezbi = 18
            vjezbeObjekat.brojZadataka = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
            VjezbeAjax.iscrtajVjezbe(div,vjezbeObjekat)
            assert.equal(div.childElementCount, 0)
        });
        it('Iscrtavanje decimalnog broja vježbi', function() {
            var div = document.getElementById('divInput')
            div.innerHTML=""
            var vjezbeObjekat = new Object()
            vjezbeObjekat.brojVjezbi = 1.3
            vjezbeObjekat.brojZadataka = [2]
            VjezbeAjax.iscrtajVjezbe(div,vjezbeObjekat)
            assert.equal(div.childElementCount, 0)
        });
    });
    describe ('iscrtajZadatke()', function() {
        it('Iscrtavanje validnog broja zadataka', function() {
            var div = document.getElementById('divInput')
            div.innerHTML=""
            var divVjezba = document.createElement("div")
            var dugmeVjezba = document.createElement("button")
            dugmeVjezba.innerText ="VJEŽBA"
            dugmeVjezba.addEventListener("click", function() {VjezbeAjax.iscrtajZadatke(divVjezba, 2)})
            divVjezba.appendChild(dugmeVjezba)
            div.appendChild(divVjezba)
            
            dugmeVjezba.click()

            assert.equal(dugmeVjezba.nextElementSibling.childNodes.length,2, "Neispravno iscrtani zadaci")
            
        });
        it('Iscrtavanje nula zadataka', function() {
            var div = document.getElementById('divInput')
            div.innerHTML=""
            var divVjezba = document.createElement("div")
            var dugmeVjezba = document.createElement("button")
            dugmeVjezba.innerText ="VJEŽBA"
            dugmeVjezba.addEventListener("click", function() {VjezbeAjax.iscrtajZadatke(divVjezba, 0)})
            divVjezba.appendChild(dugmeVjezba)
            div.appendChild(divVjezba)
            
            dugmeVjezba.click()

            assert.equal(dugmeVjezba.nextElementSibling.childElementCount,0, "Iscrtani su zadaci")
            
        });
        it('Iscrtavanje negativnog broja zadataka', function() {
            var div = document.getElementById('divInput')
            div.innerHTML=""
            var divVjezba = document.createElement("div")
            var dugmeVjezba = document.createElement("button")
            dugmeVjezba.innerText ="VJEŽBA"
            dugmeVjezba.addEventListener("click", function() {VjezbeAjax.iscrtajZadatke(divVjezba, -2)})
            divVjezba.appendChild(dugmeVjezba)
            div.appendChild(divVjezba)
            
            dugmeVjezba.click()

            assert.equal(dugmeVjezba.nextElementSibling,null, "Iscrtani su zadaci")
            
        });
        it('Iscrtavanje decimalnog broja zadataka', function() {
            var div = document.getElementById('divInput')
            div.innerHTML=""
            var divVjezba = document.createElement("div")
            var dugmeVjezba = document.createElement("button")
            dugmeVjezba.innerText ="VJEŽBA"
            dugmeVjezba.addEventListener("click", function() {VjezbeAjax.iscrtajZadatke(divVjezba, 6.9)})
            divVjezba.appendChild(dugmeVjezba)
            div.appendChild(divVjezba)
            
            dugmeVjezba.click()

            assert.equal(dugmeVjezba.nextElementSibling,null, "Iscrtani su zadaci")
            
        });
        it('Iscrtavanje više od 10 zadataka', function() {
            var div = document.getElementById('divInput')
            div.innerHTML=""
            var divVjezba = document.createElement("div")
            var dugmeVjezba = document.createElement("button")
            dugmeVjezba.innerText ="VJEŽBA"
            dugmeVjezba.addEventListener("click", function() {VjezbeAjax.iscrtajZadatke(divVjezba, 15)})
            divVjezba.appendChild(dugmeVjezba)
            div.appendChild(divVjezba)
            
            dugmeVjezba.click()

            assert.equal(dugmeVjezba.nextElementSibling,null, "Iscrtani su zadaci")
            
        });
        it('Testiranje otvaranja i zatvaranja vježbi', function() {
            var div = document.getElementById('divInput')
            div.innerHTML=""
            var divVjezba1 = document.createElement("div")
            var dugmeVjezba1 = document.createElement("button")
            dugmeVjezba1.innerText ="VJEŽBA 1"
            dugmeVjezba1.addEventListener("click", function() {VjezbeAjax.iscrtajZadatke(divVjezba1, 2)})
            divVjezba1.appendChild(dugmeVjezba1)

            var divVjezba2 = document.createElement("div")
            var dugmeVjezba2 = document.createElement("button")
            dugmeVjezba2.innerText ="VJEŽBA 1"
            dugmeVjezba2.addEventListener("click", function() {VjezbeAjax.iscrtajZadatke(divVjezba2, 3)})
            divVjezba2.appendChild(dugmeVjezba2)

            div.appendChild(divVjezba1)
            div.appendChild(divVjezba2)
            
            dugmeVjezba1.click()

            assert.equal(dugmeVjezba1.nextElementSibling.childNodes.length,2, "Nisu prikazani zadaci")
            assert.equal(dugmeVjezba2.nextElementSibling,null, "Prikazani su pogrešni zadaci")

            dugmeVjezba2.click()

            assert.equal(dugmeVjezba2.nextElementSibling.childNodes.length,3, "Nisu prikazani zadaci")
            assert.equal(dugmeVjezba1.nextElementSibling.style.display,"none", "Prikazani su pogrešni zadaci")

            dugmeVjezba2.click()

            assert.equal(dugmeVjezba1.nextElementSibling.style.display,"none", "Prikazani su pogrešni zadaci")
            assert.equal(dugmeVjezba2.nextElementSibling.style.display,"none", "Prikazani su pogrešni zadaci")
            
        });
    });
    describe('dohvatiPodatke()', function() {
        it('Dohvatanje validnih podataka', function(done) {
            var podaci = {brojVjezbi: 3, brojZadataka: [2,4,7]}
            var dataJson = JSON.stringify(podaci)
            VjezbeAjax.dohvatiPodatke(function(err,data) {
                assert.equal(data.brojVjezbi,3)
                assert.equal(data.brojZadataka[0],2)
                assert.equal(data.brojZadataka[1],4)
                assert.equal(data.brojZadataka[2],7)
                done()
            })
            this.requests[0].respond(200,{'Content-Type':'text/json'}, dataJson)
        })
        it('Dohvatanje podataka s nevalidnim brojem vježbi', function(done) {
            var podaci = {brojVjezbi:20, brojZadataka:[2,4,10,5]}
            var dataJson = JSON.stringify(podaci)
            VjezbeAjax.dohvatiPodatke(function(err,data) {
                err.should.exist
                assert.equal(data,null)
                assert.equal(err,"Pogrešan parametar brojVjezbi,brojZadataka")
                done()
            })
            this.requests[0].respond(200,{'Content-Type':'text/json'}, dataJson)
        })
        it('Dohvatanje podataka s decimalnim brojem vježbi', function(done) {
            var podaci = {brojVjezbi:4.01, brojZadataka:[2,4,10,5]}
            var dataJson = JSON.stringify(podaci)
            VjezbeAjax.dohvatiPodatke(function(err,data) {
                err.should.exist
                assert.equal(data,null)
                assert.equal(err,"Pogrešan parametar brojVjezbi,brojZadataka")
                done()
            })
            this.requests[0].respond(200,{'Content-Type':'text/json'}, dataJson)
        })
        it('Dohvatanje podataka s nevalidnim brojem zadataka', function(done) {
            var podaci = {brojVjezbi:5, brojZadataka:[2,-1,10,20,4.5]}
            var dataJson = JSON.stringify(podaci)
            VjezbeAjax.dohvatiPodatke(function(err,data) {
                err.should.exist
                assert.equal(data,null)
                assert.equal(err,"Pogrešan parametar z1,z3,z4")
                done()
            })
            this.requests[0].respond(200,{'Content-Type':'text/json'}, dataJson)
        })
        it('Dohvatanje podataka s nevalidnom dužinom niza', function(done) {
            var podaci = {brojVjezbi:2, brojZadataka:[2,1,5]}
            var dataJson = JSON.stringify(podaci)
            VjezbeAjax.dohvatiPodatke(function(err,data) {
                err.should.exist
                assert.equal(data,null)
                assert.equal(err,"Pogrešan parametar brojZadataka")
                done()
            })
            this.requests[0].respond(200,{'Content-Type':'text/json'}, dataJson)
        })
    });
    describe('posaljiPodatke()', function() {
        it('Slanje podataka u vjezbe.csv', function() {
           
                let vjezbeObjekat = new Object()
                vjezbeObjekat.brojVjezbi = 2
                vjezbeObjekat.brojZadataka = new Array()
                vjezbeObjekat.brojZadataka.push(2)
                vjezbeObjekat.brojZadataka.push(5)

                let dataJson = JSON.stringify(vjezbeObjekat)
                VjezbeAjax.posaljiPodatke(vjezbeObjekat,function(err,data) {
                    err.should.not.exist
                 })

                 this.requests[0].requestBody.should.equal(dataJson);
            
        });
        it("Slanje pogrešnog broja vježbi u vjezbe.csv", function(){
                let vjezbeObjekat = new Object()
                vjezbeObjekat.brojVjezbi = 20
                vjezbeObjekat.brojZadataka = new Array()
                vjezbeObjekat.brojZadataka.push(2)
                vjezbeObjekat.brojZadataka.push(5)

                let dataJson = JSON.stringify(vjezbeObjekat)
                VjezbeAjax.posaljiPodatke(vjezbeObjekat,function(greska,data) {
                        assert.equal(data,null, "Nije postavljen objekat na null")
                        assert.equal(greska, "Pogrešan parametar brojVjezbi,brojZadataka","Nije definisana poruka greške")

                })
            
        });
        it("Slanje decimalnog broja vježbi u vjezbe.csv", function(){
            let vjezbeObjekat = new Object()
            vjezbeObjekat.brojVjezbi = 2.0001
            vjezbeObjekat.brojZadataka = new Array()
            vjezbeObjekat.brojZadataka.push(2)
            vjezbeObjekat.brojZadataka.push(5)

            let dataJson = JSON.stringify(vjezbeObjekat)
            VjezbeAjax.posaljiPodatke(vjezbeObjekat,function(greska,data) {
                    assert.equal(data,null, "Nije postavljen objekat na null")
                    assert.equal(greska, "Pogrešan parametar brojVjezbi,brojZadataka","Nije definisana poruka greške")

            })
        
        });
        
        it("Slanje pogrešnog broja zadataka u vjezbe.csv", function(){
            
                let vjezbeObjekat = new Object()
                vjezbeObjekat.brojVjezbi = 5
                vjezbeObjekat.brojZadataka = new Array()
                vjezbeObjekat.brojZadataka.push(2)
                vjezbeObjekat.brojZadataka.push(20)
                vjezbeObjekat.brojZadataka.push(10)
                vjezbeObjekat.brojZadataka.push(-7)
                vjezbeObjekat.brojZadataka.push(3.5)

                let dataJson = JSON.stringify(vjezbeObjekat)
                VjezbeAjax.posaljiPodatke(vjezbeObjekat,function(greska,data) {

                    assert.equal(data,null, "Nije postavljen objekat na null")
                    assert.equal(greska, "Pogrešan parametar z1,z3,z4","Nije definisana poruka greške")

                })
                
            
        });

        it("Slanje neodgovarajućeg niza za broj vježbi u vjezbe.csv", function(){
            
                
                let vjezbeObjekat = new Object()
                vjezbeObjekat.brojVjezbi = 2
                vjezbeObjekat.brojZadataka = new Array()
                vjezbeObjekat.brojZadataka.push(2)
                vjezbeObjekat.brojZadataka.push(10)
                vjezbeObjekat.brojZadataka.push(5)

                let dataJson = JSON.stringify(vjezbeObjekat)
                VjezbeAjax.posaljiPodatke(vjezbeObjekat,function(greska,data) {

                    assert.equal(data,null, "Nije postavljen objekat na null")
                    assert.equal(greska, "Pogrešan parametar brojZadataka","Nije definisana poruka greške")

                })
                
            
        });
    });
    
})