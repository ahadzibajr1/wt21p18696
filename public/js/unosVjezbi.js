window.onload = function() {

    var dugmePotvrdi = document.getElementById("btnVjezbe")
    dugmePotvrdi.addEventListener("click",function(e){
        var div = document.getElementById("zadaci")
        VjezbeAjax.dodajInputPolja(div,document.getElementById("brojVjezbi").value)
    })

    var dugmePosalji = document.getElementById("btnPosalji")

    dugmePosalji.addEventListener("click", function(e){
        var vjezbaObjekat = new Object()
        vjezbaObjekat.brojVjezbi = document.getElementById("brojVjezbi").value
        vjezbaObjekat.brojZadataka = new Array()
        for (let i=0; i< 15; i++) {
            var id = "z" + i
            if(document.getElementById(id)==null) break
            
            vjezbaObjekat.brojZadataka.push(document.getElementById(id).value)
            
        }
        
        VjezbeAjax.posaljiPodatke(vjezbaObjekat,function(err,data){})

    })
}
