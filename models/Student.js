const Sequelize = require("sequelize")
const sequelize = require("../db")

module.exports = function(sequelize, DataTypes) {
    const Student = sequelize.define('Student',{
        ime: Sequelize.STRING,
        prezime: Sequelize.STRING,
        index: {
            type: Sequelize.STRING,
            unique: true
        }

    })
    return Student
}