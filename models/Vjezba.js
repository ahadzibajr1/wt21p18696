const Sequelize = require("sequelize")
const sequelize = require("../db")

module.exports = function(sequelize, DataTypes) {
    const Vjezba = sequelize.define('Vjezba',{
        naziv: Sequelize.STRING
    })
    return Vjezba
}