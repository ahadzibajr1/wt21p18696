var TestoviParser = (function() {
    var dajTacnost = function(report) {
    var tacnostObject = new Object()
    var greske = new Array()

    try {
       var obj = JSON.parse(report)

       if (!obj.hasOwnProperty("failures") || !obj.hasOwnProperty("passes")) 
            throw 'Nema testova'

       for (let i in obj.failures) {
        greske.push(obj.failures[i].fullTitle)
        }

        var t = ((obj.stats.passes/(obj.stats.tests))*100).toFixed(1);
        if (!(t-parseInt(t) > 0))
             t = parseInt(t)
        tacnostObject.tacnost = t + "%"
        tacnostObject.greske = greske

        return tacnostObject
    } catch(e) {
        tacnostObject.tacnost = "0%"
        greske.push("Testovi se ne mogu izvršiti")
        tacnostObject.greske = greske
        return tacnostObject
    }
     
    
}

var porediRezultate= function(rezultat1, rezultat2) {
    try {
        var poredbaObj = new Object()
        var obj1 = JSON.parse(rezultat1)
        var obj2 = JSON.parse(rezultat2)

        if (!obj1.hasOwnProperty("failures") || !obj2.hasOwnProperty("failures") || 
            !obj1.hasOwnProperty("passes") || !obj2.hasOwnProperty("passes")) 
            throw 'Nema testova'
        
        
        var niz1 = new Array()

        for (let i in obj1.tests)
            niz1.push(obj1.tests[i].fullTitle)

        var niz2 = new Array()

        for (let i in obj2.tests)
            niz2.push(obj2.tests[i].fullTitle)

        var sortirano1 = niz1.sort()
        var sortirano2 = niz2.sort()
        if (JSON.stringify(sortirano1) == JSON.stringify(sortirano2)) {
            poredbaObj.promjena = dajTacnost(rezultat2).tacnost
            poredbaObj.greske = dajTacnost(rezultat2).greske.sort()

            return poredbaObj
        } else {
            var padoviURezultatu1 = dajTacnost(rezultat1).greske

           for (let i in obj2.tests)
                for(let j in padoviURezultatu1)
                        if (obj2.tests[i].fullTitle==padoviURezultatu1[j])
                            padoviURezultatu1.splice(j,1)

            var x = ((padoviURezultatu1.length+obj2.stats.failures)/(padoviURezultatu1.length+obj2.stats.tests)*100).toFixed(1)

            if (!(x - parseInt(x) >0))
                x = parseInt(x)

            poredbaObj.promjena = x + "%"
            padoviURezultatu1 = padoviURezultatu1.sort()
            var padoviURezultatu2 = dajTacnost(rezultat2).greske
            padoviURezultatu2 = padoviURezultatu2.sort()
            poredbaObj.greske = padoviURezultatu1.concat(padoviURezultatu2)

            return poredbaObj

        }
        
    } catch(e) {
            poredbaObj.promjena = "0%"
            poredbaObj.greske = new Array("Testovi se ne mogu izvršiti");
            return poredbaObj
    }
}

return {
    dajTacnost: dajTacnost,
    porediRezultate: porediRezultate
}

}());