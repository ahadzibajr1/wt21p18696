const express = require('express');
const { contentType } = require('express/lib/response');
const bodyParser = require('body-parser')
const Sequelize = require('sequelize');
const sequelize = require(__dirname+'/db');

const Student = require(__dirname+"/models/Student")(sequelize)
const Grupa = require(__dirname+"/models/Grupa")(sequelize)
const Vjezba = require(__dirname+"/models/Vjezba")(sequelize)
const Zadatak= require(__dirname+"/models/Zadatak")(sequelize)

Grupa.hasMany(Student,{as:'studentiGrupe'})

Vjezba.hasMany(Zadatak,{as:'zadaciVjezbe'})

sequelize.sync()

const app = express()

app.use(express.static(__dirname+'/public'))
app.use(express.static(__dirname+'/public/html'))
app.use(express.static(__dirname+'/public/css'))
app.use(express.static(__dirname+'/public/js'))
app.use(express.static(__dirname+'/public/csv'))
app.use(bodyParser.json())
app.use(bodyParser.text())





app.get('/vjezbe', function(req,res) {
    var brojVjezbi = 0;
    var nizZadataka = []
    var greska = "Pogrešan parametar "
    var brojac = 0

    Vjezba.findAndCountAll().then( result=> {
        brojVjezbi = result.count
        nizZadataka = new Array(brojVjezbi)

        if(brojVjezbi>15 || brojVjezbi<1) {
            greska+="brojVjezbi"
        }

        result.rows.forEach( function (vjezba,index) {
            Zadatak.findAndCountAll({where: {VjezbaId: vjezba.id}}).then( function(zad) {
             nizZadataka[index] = zad.count
            
            if(zad.count <0 || zad.count>10) {
                if(greska.length >=20)
                    greska+=",z" + brojac
                else
                    greska+="z" + brojac
            }

            brojac++
    
            if(!nizZadataka.includes(undefined)) {
                console.log(nizZadataka)
                if(greska.length>=20) {
                    res.status(500)
                    res.send(greska)
                    return
                } else {

                var odg = new Object();
                odg.brojVjezbi =  brojVjezbi;
                odg.brojZadataka = nizZadataka;
                res.status(200);
                res.header('Content-Type','application/json');
                res.json(odg);
                return
                }
            }
        })
        })

        if(brojVjezbi == 0) {
            var odg = new Object();
                odg.brojVjezbi =  brojVjezbi;
                odg.brojZadataka = nizZadataka;
                res.status(200);
                res.header('Content-Type','application/json');
                res.json(odg);
                
        }


    }).catch(error=> console.log(error))

    

    /*if(brojVjezbi != nizZadataka.length)
            if(greska.length >=20)
                greska+=",brojZadataka" 
            else
                greska+="brojZadataka" */

    
});

app.post('/vjezbe', function(req,res){
    var odgGreska = new Object();
    var greske = 0;
    var unos = "vjezba,brojZadataka";
    var body = req.body;
    odgGreska.data = "Pogrešan parametar "

    if (body["brojVjezbi"] < 1 || body["brojVjezbi"] >15) {
        odgGreska.status = 'error'
        odgGreska.data += "brojVjezbi"
        greske++
    }

    
    for (let i =0; i< body["brojZadataka"].length; i++) {
        if (body["brojZadataka"][i] < 0 || body["brojZadataka"][i]>10){
            if (greske>0)
                odgGreska.data += ",z" + i
            else
                odgGreska.data+="z" + i
            greske++
        } else {
            unos +="\nvjezba" + i +"," + body["brojZadataka"][i]
        }
    }

    if (body["brojVjezbi"] != body["brojZadataka"].length) {
        if(greske>0)
            odgGreska.data += ",brojZadataka"
        else
            odgGreska.data += "brojZadataka"
        greske++
    }

    if (greske >0){
        res.status(500)
        res.header('Content-Type','application/json')
        res.json(odgGreska)
        return
    } else {

    //nisu pogresni podaci
    var odg = new Object();
    odg.brojVjezbi = body["brojVjezbi"]
    odg.brojZadataka = body["brojZadataka"]

    
    Vjezba.destroy({where:{},truncate:{cascade: true}}).then(()=> {
        Zadatak.destroy({where:{},truncate:{cascade: true}}).then(()=>{
        var vjezbe = []
        let brojac = 0;
        
        for (let i=1; i<=odg.brojVjezbi; i++) {
            let nazivVjezbe = "Vježba " + i
            vjezbe.push({naziv: nazivVjezbe})
        }

        Vjezba.bulkCreate(vjezbe).then(
        ()=>   {return Vjezba.findAll()}
        ).then( function (vjezbe){
                vjezbe.forEach( (v,index) => {
                    var zadaciPromises = []
                    for (let j=1; j<=odg.brojZadataka[index]; j++) {
                        let nazivZadatka = "Zadatak " + j
                        zadaciPromises.push(v.createZadaciVjezbe({naziv: nazivZadatka}))
                    }
                    return Promise.all(zadaciPromises)
                    
                    
                })
        })
    }).then(result => {
            res.status(200)
            res.header('Content-Type','application/json')
            res.json(odg)
        }).catch(err => console.log(err))

    }).catch(error => console.log(error))
    
    
    }       
});


app.post('/student', function(req,res) {
    var student = req.body
    if (student.ime == undefined || student.ime.trim().length == 0 || student.prezime == undefined
        || student.prezime.trim().length == 0 || student.index == undefined || student.index.trim().length == 0
        || student.grupa == undefined || student.grupa.trim().length == 0) {
            res.status(500)
            res.header('Content-type', 'application/json')
            res.send({status: "Nisu uneseni svi podaci za studenta!"})
            return
        }

    Student.findAndCountAll({where: {index: student.index}}).then(
        result => {
            if (result.count > 0) {
                var response = new Object()
                response.status = "Student sa indexom " + student.index + " već postoji!"
                res.send(response)
                return
            } else {
                Grupa.findAndCountAll({where: {naziv: student.grupa}}).then(
                    grupe => {
                        if(grupe.count > 0) {
                            Student.create({ime: student.ime, prezime: student.prezime, index: student.index,
                               GrupaId: grupe.rows[0].id }).then(
                                   () => {
                                    var response = new Object()
                                    response.status = "Kreiran student!"
                                    res.send(response)
                                    return
                                   }
                               )
                        } else {
                            Grupa.create({naziv: student.grupa}).then(grp =>
                                {
                                    grp.createStudentiGrupe({ime: student.ime, prezime: student.prezime, 
                                        index: student.index}).then(()=>{
                                            var response = new Object()
                                            response.status = "Kreiran student!"
                                            res.send(response)
                                            return
                                        })
                                })
                        }
                    }
                )
            }
        }
    
    ).catch(err => console.log(err))
});


app.put('/student/:index', function(req,res) {
    if(req.body.grupa == undefined || req.body.grupa.trim().length==0) {
        res.status(500)
        res.header('Content-type', 'application/json')
        res.send({status:"Nije unesena grupa!"})
        return
    }

    Student.findOne({where: {index: req.params.index}}).then(
        student => {
            if (student != undefined) {
                Grupa.findOne({where: {naziv: req.body.grupa}}).then(
                    grupa => {
                        if (grupa!= undefined) {
                            Student.update({GrupaId: grupa.id},{where: {id: student.id}}).then(
                            ()=> {
                            let odg = new Object()
                            odg.status ="Promjenjena grupa studentu " + req.params.index
                            res.status(200)
                            res.header('Content-type', 'application/json')
                            res.send(odg)
                            return
                            })} else {
                                Grupa.create({naziv: req.body.grupa}).then(
                                    grupa => {
                                        Student.update({GrupaId: grupa.id},{where: {id: student.id}}).then(
                                            ()=> {
                                            let odg = new Object()
                                            odg.status ="Promjenjena grupa studentu " + req.params.index
                                            res.status(200)
                                            res.header('Content-type', 'application/json')
                                            res.send(odg)
                                            return
                                            })
                                    }
                                )
                            }
                        }
                )} else {
                    let odg = new Object()
                    odg.status="Student sa indexom " + req.params.index + " ne postoji"
                    res.status(404)
                    res.header('Content-type', 'application/json')
                    res.send(odg)
                    return

                }
            }
    ).catch(error => console.log(error))
});


app.post('/batch/student', function (req,res){
var nizStudenata = req.body.split(/[\r\n,]/).filter(s => s.length>0)

    var postojeciIndexi = []
    var postojeciIndexi2 = []
    var sviIndexi = []
    var promises = []
    var promises2 = []
    var promises3 = []

    var studenti = []
    var studentiZaDodati = []
    var grupe = []
    var indexi = []

    for (let i=0; i<nizStudenata.length; i+=4) {
        if(i+1>= nizStudenata.length || i+2 >= nizStudenata.length || i+3 >= nizStudenata.length) {
            res.status(500)
            res.header('Content-type', 'application/json')
            res.send({status: 'Nisu uneseni svi podaci za studente!'})
            return
        }

        if (nizStudenata[i].length==0 || nizStudenata[i+1].length==0 ||
        nizStudenata[i+2].length==0 || nizStudenata[i+3].length==0) {
            res.status(500)
            res.header('Content-type', 'application/json')
            res.send({status: 'Nisu uneseni svi podaci za studente!'})
            return
        }

        if(!indexi.includes(nizStudenata[i+2])) {
            studenti.push({ime:nizStudenata[i],prezime:nizStudenata[i+1],index:nizStudenata[i+2],
            grupa:nizStudenata[i+3]})
            indexi.push(nizStudenata[i+2])
        } else {
            postojeciIndexi.push(nizStudenata[i+2])
        }

        sviIndexi.push(nizStudenata[i+2])
    }


    
    studenti.forEach( s => {
        promises.push(Student.findOne({where: {index: s.index}})
            
        )
    })
    
    Promise.all(promises).then( (students)=> {
        students.forEach((st,index)=> {
            if(st == null) {
                studentiZaDodati.push(studenti[index])
                grupe.push(studenti[index].grupa)
            } else {
                postojeciIndexi2.push(st.index)
            }
        })

        grupe = [...new Set(grupe)]
        
        grupe.forEach(gr=> {
            promises2.push(Grupa.findOrCreate({where: {naziv:gr}}))
        })
        return Promise.all(promises2)
    }).then(()=> {
           
            return Grupa.findAll()
            
    }).then((groups) =>{
            
            groups.forEach(grp => {
                studentiZaDodati.forEach( student => {
                if(student.grupa == grp.naziv)
                    promises3.push(grp.createStudentiGrupe({ime:student.ime,prezime:student.prezime,index:student.index}))
                })
            })
            
        return Promise.all(promises3)
    }).then( ()=> {
        
            let postojeciIndexi3 = []

            
            for(let i=0; i<sviIndexi.length; i++) {
               
                if (postojeciIndexi.includes(sviIndexi[i]) || postojeciIndexi2.includes(sviIndexi[i]))
                    postojeciIndexi3.push(sviIndexi[i])    
            }

            let postojeciDodani = []

            postojeciIndexi.forEach( pi=> {
                if(!postojeciIndexi2.includes(pi) && !postojeciDodani.includes(pi)) 
                    postojeciDodani.push(pi)
            })


            while(postojeciDodani.length > 0) {
                for(let i=0; i<postojeciIndexi3.length;i++) {
                    if(postojeciDodani.includes(postojeciIndexi3[i])) {
                        ind2 = postojeciDodani.findIndex(x => x ==postojeciIndexi3[i])
                        postojeciIndexi3.splice(i,1)
                        postojeciDodani.splice(ind2,1)
                        break
                    }
                }
            }
                
            res.status(200)
            res.header('Content-type', 'application/json')
            
            if (postojeciIndexi3.length == 0)
                res.send({status: "Dodano " + (nizStudenata.length/4) + " studenata!"})
            else {
                let odg = "Dodano " + (studentiZaDodati.length) + " studenata, a studenti "
                postojeciIndexi3.forEach( (i,index) => {
                    if (index < postojeciIndexi3.length-1)
                        odg += i + ","
                    else
                        odg += i
                })
                odg += " već postoje!"
                
                res.send({status: odg})
            }
        }
    ).catch(err => console.log(err))
                    
          

    
});

var server = app.listen(3000)
module.exports = server